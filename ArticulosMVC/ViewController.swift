//
//  ViewController.swift
//  ArticulosMVC
//
//  Created by Salvador Lopez on 29/06/23.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var articulos = [Articulo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 120
        loadData(url: URL(string: Constantes.restApiUrl)!)
    }
    
    func loadData(url:URL){
        URLSession.shared.dataTask(with: url){
            data, resp, error in
            if let error = error{
                print("Error: \(error)")
            }else if let data = data {
                //print(String(data: data, encoding: .utf8))
                do{
                    let articleList = try JSONDecoder().decode(ArticuloLista.self, from: data)
                    //dump(articleList)
                    self.articulos = articleList.articles
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }catch{
                    print("Error: \(error)")
                }
            }
        }.resume()
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articulos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyCustomTableViewCell
        cell.titleArticle.text = articulos[indexPath.row].title
        cell.detailArticle.text = articulos[indexPath.row].description
        return cell
    }
    
    
}
