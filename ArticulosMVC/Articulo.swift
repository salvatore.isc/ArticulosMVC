//
//  Articulo.swift
//  ArticulosMVC
//
//  Created by Salvador Lopez on 29/06/23.
//

import Foundation

struct ArticuloLista: Decodable{
    let articles: [Articulo]
}

struct Articulo: Decodable{
    let title: String?
    let description: String?
}
