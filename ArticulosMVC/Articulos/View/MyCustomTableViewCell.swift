//
//  MyCustomTableViewCell.swift
//  ArticulosMVC
//
//  Created by Salvador Lopez on 29/06/23.
//

import UIKit

class MyCustomTableViewCell: UITableViewCell {

    @IBOutlet weak var detailArticle: UILabel!
    @IBOutlet weak var titleArticle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
